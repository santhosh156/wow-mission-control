import pyodbc
import cx_Oracle
import datetime, time
import pymysql
from sshtunnel import SSHTunnelForwarder

class DBUtils:
	""" Class with variables and methods for all database actions """

	def __init__(self, logger, config):
		"""
			name:	__init__
			desc:	DBUtils constructor - Logger and config objects passed from main
			args:	logger - logger object from main
				  	config - config object from main
			return:	None
		"""
		self.logger = logger
		self.config = config

	def db_connect_mssql(self, server, database):
		"""
			name:	db_connect_mssql
			desc:	Method to connect to SQLSERVER database
			args:	server - MSSQL database server
				  	database - Database to connect
			return:	None
		"""
		try:
			connectionString = "Driver={ODBC Driver 11 for SQL Server};Server=" + server + ";Database=" + database + ";Trusted_Connection=yes;"
			connection = pyodbc.connect(connectionString)
			self.logger.debug("Connected to {}".format(server))
			return connection
		except Exception as e:
			self.logger.exception(str(e))

	def db_connect_oracle(self, server, port, username, password, serviceName=None, sid=None ):
		"""
			name:	db_connect_oracle
			desc:	Method to connect to Oracle database
			args:	server - Oracle database server
				  	database - Database to connect
			return:	None
		"""
		try:
			if serviceName != None:
				dsn_tns = cx_Oracle.makedsn(server, port, service_name=serviceName)
			elif sid != None:
				dsn_tns = cx_Oracle.makedsn(server, port, sid=sid)
			else:
				raise cx_Oracle.DatabaseError

			connection = cx_Oracle.connect(user=username, password=password, dsn=dsn_tns)
			self.logger.debug("Connected to {}".format(server))
			return connection
		except Exception as e:
			self.logger.exception(str(e))

	def db_run_query_ssh_mysql(self, sshHost, sshPort, sshUser, sqlHost, sqlPort, sqlUser, sqlPwd, sqlDatabase, sshPvtKey, sqlQuery):
		"""
			name:	db_connect_mysql
			desc:	Method to connect to MYSQL database
			args:	server - MYSQL database server
				  	database - Database to connect
			return:	None
		"""
		try:
			tunnel = SSHTunnelForwarder((sshHost, sshPort), ssh_username=sshUser, ssh_pkey=sshPvtKey, remote_bind_address=(sqlHost, sqlPort))
			tunnel.start()
			print('Establishing connection')
			print('tunnel_bindings:', tunnel.tunnel_bindings)
			print('tunnel_is_up:', tunnel.tunnel_is_up)
			print('local_bind_port:', tunnel.local_bind_port)
			self.logger.debug("Connected thru SSH tunnel to {}".format(sshHost))
			connection = pymysql.connect(host=sqlHost, user=sqlUser, passwd=sqlPwd, db=sqlDatabase, port=tunnel.local_bind_port, cursorclass=pymysql.cursors.DictCursor)

			with connection.cursor() as cursor:
				self.logger.debug("Connected to mysql database {}".format(sqlDatabase))
				#cursor = conn.cursor()
				cursor.execute(sqlQuery)
				#cursor.execute("SELECT VERSION();")
				results = cursor.fetchone()
			print(results)
			self.logger.debug("Connection closed to mysql database {}".format(sqlDatabase))

			self.logger.debug("Connection thru SSH tunnel closed to {}".format(sshHost))
			return list(results)
		except Exception as e:
			self.logger.exception(str(e))
		finally:
			connection.close()
			tunnel.terminate()
