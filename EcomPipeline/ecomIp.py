
class EcomIp():
    """ Class with variables and methods for Ecom IP """

    def __init__(self, logger, config, dbUtils):
        """
            name:	__init__
            desc:	EcomIp constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils

    def get_ecom_ip_metrics(self):
        """
            name:	get_ecom_ip_metrics
            desc:	Method that gets all the ecom ip metrics
            args:	None
            return:	Ecom ip metrics dictionary
        """
        try:
            ecomIpMetrics = {}

            ecomIpMetrics['BubbleId'] = self.config.ipBubbleId
            ecomIpMetrics['Application'] = self.config.ipApplication
            ecomIpMetrics['Status'] = 'AVAILABLE'

            ecomIpMetrics['Orders Dropped'] = "NA"
            ecomIpMetrics['Order Units'] = "NA"
            ecomIpMetrics['Orders Shipped'] = "NA"
            ecomIpMetrics['Order Units Shipped'] = "NA"

            return ecomIpMetrics
        except Exception as e:
            self.logger.exception(str(e))
