import sys
import os
import logging
import logging.handlers
import datetime
import json
from ecomConfig import EcomConfig
from ecomPipeline import EcomPipeline
import time

#Instantiating config to load app configurations
config = EcomConfig()

sys.path.append(config.libDir)
from dbutils import DBUtils
from utils import Utils

# creating logger
logger = logging.getLogger(config.appName)

# setting up logging level
loggingLevel = logging.DEBUG

# create log formatter
formatter = logging.Formatter(fmt=config.appLogFormat, datefmt=config.appLogDateFormat)

# create file handler and set level to debug
handler = logging.handlers.TimedRotatingFileHandler(config.applogFile, when="W0", interval=7, backupCount=30)
handler.setFormatter(formatter)
handler.setLevel(loggingLevel)
logger.addHandler(handler)
logger.setLevel(loggingLevel)

# Instantiate the dashboard
generalUtils = Utils(logger, config)

# Instantiate the dashboard
dbUtils = DBUtils(logger, config)

# Instantiate the dashboard
ecomPipeline = EcomPipeline(logger, config, generalUtils, dbUtils)

try:
    #while True:
    logger.info("Started - Ecom pipeline ")

    # Loading the ecom pipeline data json template to a variable
    ecomData = json.load(open('ecom_pipeline.json'))
    ecomData = dict(ecomData)
    print(ecomData)
    # Loading the latest ecom pipeline data json file to the same variable
    ecomData["results"] = json.loads(ecomPipeline.build_ecom_output())
    print(ecomData)
    # Writes all the ecom metrics from EcomPipeline objects the JSON to be consumed by UI
    with open(config.ecomJsonFile, 'w') as outFile:
        outFile.write(json.dumps(ecomData, indent=4))
        logger.debug("Ecom pipeline data written to JSON file")

    logger.info("Completed - Ecom pipeline ")
    #time.sleep(120);
except Exception as e:
    logger.error(str(e))
