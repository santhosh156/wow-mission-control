import os

class EcomAzure():
    """ Class with variables and methods for Ecom azure """

    def __init__(self, logger, config, dbUtils):
        """
            name:	__init__
            desc:	EcomAzure constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils

    def get_ecom_azure1_metrics(self):
        """
            name:	get_ecom_azure_metrics
            desc:	Method that gets all the ecom azure metrics
            args:	None
            return:	ecom azure metrics dictionary
        """
        try:
            azureAllocMetrics = {}
            azureAllocMetrics['BubbleId'] = self.config.azure1BubbleId
            azureAllocMetrics['Application'] = self.config.azureApplication
            azureAllocMetrics['Status'] = 'AVAILABLE'

            azureAllocMetrics['Orders Dropped'] = "NA"#str(0)
            azureAllocMetrics['Order Units'] = "NA"#str(0)
            azureAllocMetrics['Orders Shipped'] = "NA"#str(AllocAzure.get_pending_alloc_files(self))
            azureAllocMetrics['Order Units Shipped'] = "NA"#str(AllocAzure.get_pending_alloc_files(self))
            return azureAllocMetrics
        except Exception as e:
            self.logger.exception(str(e))

    def get_ecom_azure2_metrics(self):
        """
            name:	get_ecom_azure_metrics
            desc:	Method that gets all the ecom azure metrics
            args:	None
            return:	ecom azure metrics dictionary
        """
        try:
            azureAllocMetrics = {}
            azureAllocMetrics['BubbleId'] = self.config.azure2BubbleId
            azureAllocMetrics['Application'] = self.config.azureApplication
            azureAllocMetrics['Status'] = 'AVAILABLE'

            azureAllocMetrics['Orders Dropped'] = "NA"#str(0)
            azureAllocMetrics['Order Units'] = "NA"#str(0)
            azureAllocMetrics['Orders Shipped'] = "NA"#str(AllocAzure.get_pending_alloc_files(self))
            azureAllocMetrics['Order Units Shipped'] = "NA"#str(AllocAzure.get_pending_alloc_files(self))
            return azureAllocMetrics
        except Exception as e:
            self.logger.exception(str(e))

    def get_pending_alloc_files(self):
        """
            name:	get_pending_alloc_files
            desc:	Method that gets all the pending files in azure  Incoming folder
            args:	None
            return:	number of pending files to be processed in azure
        """
        try:
            return 0
            '''self.logger.info("Started - Retrieval of count allocation files from azure  ")

            # Assigning the target file path location
            serverFilePath =  self.config.azureIncomingPath
            count = 0

            # Fetching all the files in destination directory ending with .dat
            for file in os.listdir(serverFilePath):
                if file.endswith(".dat"):
                    # Incrementing the count for each .dat file found in the destination folder
                    count += 1

            self.logger.info("Completed - Retrieval of count allocation files from azure  ")

            # Returning the total no of files with extension .dat
            return count'''
        except Exception as e:
            self.logger.exception(str(e))
