import os

class EcomMagentoSettle():
    """ Class with variables and methods for Ecom Magento Settle """

    def __init__(self, logger, config, dbUtils):
        """
            name:	__init__
            desc:	EcomMagentoSettle constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils

    def get_ecom_magento_settle_metrics(self):
        """
            name:	get_ecom_magento_settle_metrics
            desc:	Method that gets all the magento settle metrics
            args:	None
            return:	magento settle metrics dictionary
        """
        try:
            ecomMagentoSettleMetrics = {}

            ecomMagentoSettleMetrics['BubbleId'] = self.config.magentoSettleBubbleId
            ecomMagentoSettleMetrics['Application'] = self.config.magentoSettleApplication
            ecomMagentoSettleMetrics['Status'] = 'AVAILABLE'

            ecomMagentoSettleMetrics['Orders Dropped'] = "NA"
            ecomMagentoSettleMetrics['Order Units'] = "NA"
            ecomMagentoSettleMetrics['Orders Shipped'] = "NA"
            ecomMagentoSettleMetrics['Order Units Shipped'] = "NA"

            return ecomMagentoSettleMetrics
        except Exception as e:
            self.logger.exception(str(e))
