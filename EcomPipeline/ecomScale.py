import os

class EcomScale():
    """ Class with variables and methods for SCALE ecom """

    def __init__(self, logger, config, dbUtils):
        """
            name:	__init__
            desc:	EcomScale constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils

    def get_ecom_scale_metrics(self):
        """
            name:	get_ecom_scale_metrics
            desc:	Method that gets all the scale ecom metrics
            args:	None
            return:	scale ecom metrics dictionary
        """
        try:
            ecomScaleMetrics = {}

            ecomScaleMetrics['BubbleId'] = self.config.scaleBubbleId
            ecomScaleMetrics['Application'] = self.config.scaleApplication
            ecomScaleMetrics['Status'] = 'AVAILABLE'

            conn = self.dbUtils.db_connect_mssql(self.config.scaleDbHost, self.config.scaleDatabase)
            cursor = conn.cursor()
            result = cursor.execute(self.config.scaleEcomQuery).fetchone()
            metrics = list(result)

            ecomScaleMetrics['Orders Dropped'] = str(0 if metrics[0] is None else metrics[0])
            ecomScaleMetrics['Order Units'] = str(0 if metrics[1] is None else metrics[1])
            ecomScaleMetrics['Orders Shipped'] = str(0 if metrics[2] is None else metrics[2])
            ecomScaleMetrics['Order Units Shipped'] = str(0 if metrics[3] is None else metrics[3])

            return ecomScaleMetrics
        except Exception as e:
            self.logger.exception(str(e))
