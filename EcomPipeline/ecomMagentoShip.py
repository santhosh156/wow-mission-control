import os

class EcomMagentoShip():
    """ Class with variables and methods for Ecom Magento Ship """

    def __init__(self, logger, config, dbUtils):
        """
            name:	__init__
            desc:	EcomMagentoShip constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils

    def get_ecom_magento_ship_metrics(self):
        """
            name:	get_ecom_magento_ship_metrics
            desc:	Method that gets all the magento ship confirm metrics
            args:	None
            return:	magento ship confirm metrics dictionary
        """
        try:
            ecomMagentoShipMetrics = {}

            ecomMagentoShipMetrics['BubbleId'] = self.config.magentoShipBubbleId
            ecomMagentoShipMetrics['Application'] = self.config.magentoShipApplication
            ecomMagentoShipMetrics['Status'] = 'AVAILABLE'

            ecomMagentoShipMetrics['Orders Dropped'] = "NA"
            ecomMagentoShipMetrics['Order Units'] = "NA"
            ecomMagentoShipMetrics['Orders Shipped'] = "NA"
            ecomMagentoShipMetrics['Order Units Shipped'] = "NA"

            return ecomMagentoShipMetrics
        except Exception as e:
            self.logger.exception(str(e))
