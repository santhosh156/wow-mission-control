import os
import paramiko

class EcomMagentoDemand():
    """ Class with variables and methods for Ecom magento demand """

    def __init__(self, logger, config, genUtils, dbUtils):
        """
            name:	__init__
            desc:	EcomMagentoDemand constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils
        self.genUtils = genUtils

    def get_mgt_dmd_ecom_metrics(self):
        """
            name:	get_mgt_dmd_ecom_metrics
            desc:	Method that gets all the Ecom magento demand metrics
            args:	None
            return:	Ecom magento demand metrics dictionary
        """
        try:
            ecomMgtDmdMetrics = {}

            ecomMgtDmdMetrics['BubbleId'] = self.config.magentoDmdBubbleId
            ecomMgtDmdMetrics['Application'] = self.config.magentoDmdApplication
            ecomMgtDmdMetrics['Status'] = 'AVAILABLE'

            privateKey = paramiko.RSAKey.from_private_key_file(self.config.magentoPvtKeyLoc)
            #metrics = self.dbUtils.db_run_query_ssh_mysql(self.config.magentoSshHost, self.config.magentoSshPort, self.config.magentoSshUser, self.config.magentoSqlHost, self.config.magentoSqlPort, self.config.magentoSqlUser, self.config.magentoSqlPwd, self.config.magentoDatabase, privateKey, self.config.magentoDmdQuery)

            ecomMgtDmdMetrics['Order Count'] = "NA"#str(0 if metrics[0] is None else metrics[0])
            ecomMgtDmdMetrics['Orders Closed'] = "NA"#str(0 if metrics[1] is None else metrics[1])
            ecomMgtDmdMetrics['Orders Cancelled'] = "NA"#str(0 if metrics[2] is None else metrics[2])
            ecomMgtDmdMetrics['Orders Onhold'] = "NA"#str(0 if metrics[3] is None else metrics[3])
            ecomMgtDmdMetrics['Sent for Fulfilment'] = "NA"#str(0 if metrics[4] is None else metrics[4])
            ecomMgtDmdMetrics['Order Sales Amount'] = "NA"#str(0 if metrics[5] is None else metrics[5])

            return ecomMgtDmdMetrics
        except Exception as e:
            self.logger.exception(str(e))
