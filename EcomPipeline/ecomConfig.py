import json
import os, sys

class EcomConfig:

    def __init__(self):
        ecomConfigFile = 'ecomConfig.json'
        with open(ecomConfigFile) as configFile:
            self.configData = json.load(configFile)

        EcomConfig.appName = self.configData["ECOM_APP_NAME"]
        EcomConfig.appDir = os.path.abspath(os.path.dirname(sys.modules['__main__'].__file__))
        EcomConfig.libDir = self.configData["LIB_DIR"]
        EcomConfig.appLogPath = os.path.join(EcomConfig.appDir, self.configData["APP_LOG_PATH"])
        EcomConfig.appLogFormat = self.configData["APP_LOG_FORMAT"]
        EcomConfig.appLogDateFormat = self.configData["APP_LOG_DATE_FORMAT"]
        EcomConfig.applogFile = os.path.join(EcomConfig.appLogPath, self.configData["APP_LOG_NAME"])
        EcomConfig.ecomJsonFile = self.configData["ECOM_JSON_FILE"]
        EcomConfig.predricktown = self.configData["DC_PREDRICKTOWN"]
        EcomConfig.magentoDmdBubbleId = self.configData["MAGENTO_DEMAND_BUBBLEID"]
        EcomConfig.magentoDmdApplication = self.configData["MAGENTO_DEMAND_APPLICATION"]
        EcomConfig.magentoSshHost = self.configData["MAGENTO_SSH_HOST"]
        EcomConfig.magentoSshPort = self.configData["MAGENTO_SSH_PORT"]
        EcomConfig.magentoSshUser = self.configData["MAGENTO_SSH_USER"]
        EcomConfig.magentoPvtKeyLoc = self.configData["MAGENTO_PRIVATE_KEY_LOCATION"]
        EcomConfig.magentoSqlHost = self.configData["MAGENTO_SQL_HOST"]
        EcomConfig.magentoSqlPort = self.configData["MAGENTO_SQL_PORT"]
        EcomConfig.magentoSqlUser = self.configData["MAGENTO_SQL_USER"]
        EcomConfig.magentoSqlPwd = self.configData["MAGENTO_SQL_PASSWORD"]
        EcomConfig.magentoDatabase = self.configData["MAGENTO_DATABASE"]
        EcomConfig.magentoDmdQuery = self.configData["MAGENTO_DEMAND_QUERY"]
        EcomConfig.scaleBubbleId = self.configData["SCALE_BUBBLEID"]
        EcomConfig.scaleApplication = self.configData["SCALE_APPLICATION"]
        EcomConfig.scaleDbHost = self.configData["SCALE_DB_HOST"]
        EcomConfig.scaleDatabase = self.configData["SCALE_DATABASE"]
        EcomConfig.scaleEcomQuery = self.configData["SCALE_ECOM_QUERY"]
        EcomConfig.azure1BubbleId = self.configData["AZURE1_BUBBLEID"]
        EcomConfig.azure2BubbleId = self.configData["AZURE2_BUBBLEID"]
        EcomConfig.azureApplication = self.configData["AZURE_APPLICATION"]
        EcomConfig.magentoShipBubbleId = self.configData["MAGENTO_SHIP_BUBBLEID"]
        EcomConfig.magentoShipApplication = self.configData["MAGENTO_SHIP_APPLICATION"]
        EcomConfig.worldpayBubbleId = self.configData["WORLDPAY_BUBBLEID"]
        EcomConfig.worldpayApplication = self.configData["WORLDPAY_APPLICATION"]
        EcomConfig.magentoSettleBubbleId = self.configData["MAGENTO_SETTLE_BUBBLEID"]
        EcomConfig.magentoSettleApplication = self.configData["MAGENTO_SETTLE_APPLICATION"]
        EcomConfig.ipBubbleId = self.configData["IP_BUBBLEID"]
        EcomConfig.ipApplication = self.configData["IP_APPLICATION"]
