import json
import sys
from ecomMagentoDemand import EcomMagentoDemand
from ecomAzure import EcomAzure
from ecomScale import EcomScale
from ecomMagentoShip import EcomMagentoShip
from ecomMagentoSettle import EcomMagentoSettle
from ecomWorldpay import EcomWorldpay
from ecomIp import EcomIp

class EcomPipeline():
    """ Class with variables and methods for Ecom Pipeline """

    def __init__(self, logger, config, generalUtils, dbUtils):
        """
            name:	__init__
            desc:	EcomPipeline constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils
        self.generalUtils = generalUtils
        self.ecomMagentoDemand = EcomMagentoDemand(logger, config, generalUtils, dbUtils)
        self.ecomAzure = EcomAzure(logger, config, dbUtils)
        self.ecomScale = EcomScale(logger, config, dbUtils)
        self.ecomMgtShip = EcomMagentoShip(logger, config, dbUtils)
        self.ecomWorldpay = EcomWorldpay(logger, config, dbUtils)
        self.ecomMgtSettle = EcomMagentoSettle(logger, config, dbUtils)
        self.ecomIp = EcomIp(logger, config, dbUtils)

    def build_ecom_output(self):
        """
            name:	build_ecom_output
            desc:	Method that builds the output ecom pipeline
            args:	None
            return:	Ecom pipeline output in JSON format
        """
        try:
            ecomPipelineOutput = []
            self.logger.info("Started - Building ecom pipeline output")
            ecomPipelineOutput.append(self.ecomMagentoDemand.get_mgt_dmd_ecom_metrics())
            ecomPipelineOutput.append(self.ecomAzure.get_ecom_azure1_metrics())
            ecomPipelineOutput.append(self.ecomScale.get_ecom_scale_metrics())
            ecomPipelineOutput.append(self.ecomAzure.get_ecom_azure2_metrics())
            ecomPipelineOutput.append(self.ecomMgtShip.get_ecom_magento_ship_metrics())
            ecomPipelineOutput.append(self.ecomWorldpay.get_ecom_worldpay_metrics())
            ecomPipelineOutput.append(self.ecomMgtSettle.get_ecom_magento_settle_metrics())
            ecomPipelineOutput.append(self.ecomIp.get_ecom_ip_metrics())

            return json.dumps(ecomPipelineOutput)
            self.logger.info("Completed - Building ecom pipeline output")
        except Exception as e:
            self.logger.exception(str(e))
