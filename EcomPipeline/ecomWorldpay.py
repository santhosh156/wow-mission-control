import os

class EcomWorldpay():
    """ Class with variables and methods for Ecom Worldpay """

    def __init__(self, logger, config, dbUtils):
        """
            name:	__init__
            desc:	EcomWorldpay constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils

    def get_ecom_worldpay_metrics(self):
        """
            name:	get_ecom_worldpay_metrics
            desc:	Method that gets all the ecom worldpay metrics
            args:	None
            return:	ecom worldpay metrics dictionary
        """
        try:
            ecomWorldpayMetrics = {}

            ecomWorldpayMetrics['BubbleId'] = self.config.worldpayBubbleId
            ecomWorldpayMetrics['Application'] = self.config.worldpayApplication
            ecomWorldpayMetrics['Status'] = 'AVAILABLE'

            ecomWorldpayMetrics['Orders Dropped'] = "NA"
            ecomWorldpayMetrics['Order Units'] = "NA"
            ecomWorldpayMetrics['Orders Shipped'] = "NA"
            ecomWorldpayMetrics['Order Units Shipped'] = "NA"

            return ecomWorldpayMetrics
        except Exception as e:
            self.logger.exception(str(e))
