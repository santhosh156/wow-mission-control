import os

class SalesEdw():
    """ Class with variables and methods for EDW sales """

    def __init__(self, logger, config, dbUtils):
        """
            name:	__init__
            desc:	SalesEdw constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils

    def get_edw_sales_metrics(self, day):
        """
            name:	get_edw_sales_metrics
            desc:	Method that gets all the edw sales metrics
            args:	None
            return:	edw sales metrics dictionary
        """
        try:
            salesEdwMetrics = {}

            if day:
                salesEdwMetrics['BubbleId'] = self.config.edwBubbleId + self.config.salesBubbleToday
            else:
                salesEdwMetrics['BubbleId'] = self.config.edwBubbleId + self.config.salesBubbleYesterday

            salesEdwMetrics['Application'] = self.config.edwApplication
            salesEdwMetrics['Status'] = 'AVAILABLE'

            conn = self.dbUtils.db_connect_mssql(self.config.edwDbServer, self.config.edwDatabase)
            with conn:
                cursor = conn.cursor()

                totalStores = list(cursor.execute(self.config.edwTotalStoresQuery[day]).fetchone())
                salesEdwMetrics['Total Stores'] = str(0 if totalStores[0] is None else totalStores[0])

                totalSales = list(cursor.execute(self.config.edwTotalSalesQuery[day]).fetchone())
                salesEdwMetrics['Total Sales $'] = str(0 if totalSales[0] is None else totalSales[0])

            return salesEdwMetrics
        except Exception as e:
            self.logger.exception(str(e))
