import json
import os, sys

class SalesConfig:

    def __init__(self):
        salesConfigFile = 'salesConfig.json'
        with open(salesConfigFile) as configFile:
            self.configData = json.load(configFile)

        SalesConfig.appName = self.configData["SALES_APP_NAME"]
        SalesConfig.salesJsonFile = self.configData["SALES_JSON_FILE"]
        SalesConfig.appDir = os.path.abspath(os.path.dirname(sys.modules['__main__'].__file__))
        SalesConfig.libDir = self.configData["LIB_DIR"]
        SalesConfig.appLogPath = os.path.join(SalesConfig.appDir, self.configData["APP_LOG_PATH"])
        SalesConfig.appLogFormat = self.configData["APP_LOG_FORMAT"]
        SalesConfig.appLogDateFormat = self.configData["APP_LOG_DATE_FORMAT"]
        SalesConfig.applogFile = os.path.join(SalesConfig.appLogPath, self.configData["APP_LOG_NAME"])
        SalesConfig.salesJsonFile = self.configData["SALES_JSON_FILE"]
        SalesConfig.salesYesterday = self.configData["SALES_YESTERDAY"]
        SalesConfig.salesToday = self.configData["SALES_TODAY"]
        SalesConfig.salesBubbleYesterday = self.configData["SALES_BUBBLE_YESTERDAY"]
        SalesConfig.salesBubbleToday = self.configData["SALES_BUBBLE_TODAY"]
        SalesConfig.storeBubbleId = self.configData["STORE_ISP_BUBBLEID"]
        SalesConfig.storeApplication = self.configData["STORE_ISP_APPLICATION"]
        SalesConfig.escBubbleId = self.configData["ESC_BUBBLEID"]
        SalesConfig.escApplication = self.configData["ESC_APPLICATION"]
        SalesConfig.aptosBubbleId = self.configData["APTOS_BUBBLEID"]
        SalesConfig.aptosApplication = self.configData["APTOS_APPLICATION"]
        SalesConfig.aptosDbServer = self.configData["APTOS_DB_HOST"]
        SalesConfig.aptosDatabase = self.configData["APTOS_DATABASE"]
        SalesConfig.aptosTotalStoresQuery = self.configData['APTOS_STORES_OPENED_QUERY']
        SalesConfig.aptosTotalSalesQuery = self.configData["APTOS_TOTAL_SALES_AMOUNT_QUERY"]
        SalesConfig.aptosNumZeroSalesStoreQuery = self.configData["APTOS_ZERO_SALES_STORES_QUERY"]
        SalesConfig.aptosHighSalesStoreQuery = self.configData["APTOS_HIGHEST_SALES_QUERY"]
        SalesConfig.dataHubBubbleId = self.configData["DATAHUB_BUBBLEID"]
        SalesConfig.dataHubApplication = self.configData["DATAHUB_APPLICATION"]
        SalesConfig.dataHubDbServer = self.configData["DATAHUB_DB_HOST"]
        SalesConfig.dataHubDatabase = self.configData["DATAHUB_DATABASE"]
        SalesConfig.dataHubTotalStoresQuery = self.configData["DATAHUB_TOTAL_STORES_QUERY"]
        SalesConfig.dataHubTotalSalesQuery = self.configData["DATAHUB_TOTAL_SALES_QUERY"]
        SalesConfig.edwBubbleId = self.configData["EDW_BUBBLEID"]
        SalesConfig.edwApplication = self.configData["EDW_APPLICATION"]
        SalesConfig.edwDbServer = self.configData["EDW_DB_HOST"]
        SalesConfig.edwDatabase = self.configData["EDW_DATABASE"]
        SalesConfig.edwTotalStoresQuery = self.configData["EDW_TOTAL_STORES_QUERY"]
        SalesConfig.edwTotalSalesQuery = self.configData["EDW_TOTAL_SALES_QUERY"]
        SalesConfig.dailyReadsBubbleId = self.configData["DAILY_READS_BUBBLEID"]
        SalesConfig.dailyReadsApplication = self.configData["DAILY_READS_APPLICATION"]
        SalesConfig.dailyReadsTotalStoresQuery = self.configData["DAILY_READS_TOTAL_STORES_QUERY"]
        SalesConfig.dailyReadsTotalSalesQuery = self.configData["DAILY_READS_TOTAL_SALES_QUERY"]
