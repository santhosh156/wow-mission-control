import os

class SalesDailyReads():
    """ Class with variables and methods for Daily reads sales """

    def __init__(self, logger, config, dbUtils):
        """
            name:	__init__
            desc:	SalesDailyReads constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils

    def get_dailyreads_sales_metrics(self, day):
        """
            name:	get_dailyreads_sales_metrics
            desc:	Method that gets all the dailyreads sales metrics
            args:	None
            return:	dailyreads sales metrics dictionary
        """
        try:
            salesDailyReadsMetrics = {}

            if day:
                salesDailyReadsMetrics['BubbleId'] = self.config.dailyReadsBubbleId + self.config.salesBubbleToday
            else:
                salesDailyReadsMetrics['BubbleId'] = self.config.dailyReadsBubbleId + self.config.salesBubbleYesterday

            salesDailyReadsMetrics['Application'] = self.config.dailyReadsApplication
            salesDailyReadsMetrics['Status'] = 'AVAILABLE'

            conn = self.dbUtils.db_connect_mssql(self.config.edwDbServer, self.config.edwDatabase)
            with conn:
                cursor = conn.cursor()

                totalStores = list(cursor.execute(self.config.dailyReadsTotalStoresQuery[day]).fetchone())
                salesDailyReadsMetrics['Total Stores'] = str(0 if totalStores[0] is None else totalStores[0])

                totalSales = list(cursor.execute(self.config.dailyReadsTotalSalesQuery[day]).fetchone())
                salesDailyReadsMetrics['Total Sales $'] = str(0 if totalSales[0] is None else totalSales[0])

            return salesDailyReadsMetrics
        except Exception as e:
            self.logger.exception(str(e))
