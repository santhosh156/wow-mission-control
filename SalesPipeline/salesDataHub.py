import os

class SalesDataHub():
    """ Class with variables and methods for phsql01 sales """

    def __init__(self, logger, config, dbUtils):
        """
            name:	__init__
            desc:	SalesDataHub constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils

    def get_datahub_sales_metrics(self, day):
        """
            name:	get_datahub_sales_metrics
            desc:	Method that gets all the phsql01 sales metrics
            args:	None
            return:	phsql01 sales metrics dictionary
        """
        try:
            salesDatahubMetrics = {}

            if day:
                salesDatahubMetrics['BubbleId'] = self.config.dataHubBubbleId + self.config.salesBubbleToday
            else:
                salesDatahubMetrics['BubbleId'] = self.config.dataHubBubbleId + self.config.salesBubbleYesterday

            salesDatahubMetrics['Application'] = self.config.dataHubApplication
            salesDatahubMetrics['Status'] = 'AVAILABLE'

            conn = self.dbUtils.db_connect_mssql(self.config.dataHubDbServer, self.config.dataHubDatabase)
            with conn:
                cursor = conn.cursor()

                totalStores = list(cursor.execute(self.config.dataHubTotalStoresQuery[day]).fetchone())
                salesDatahubMetrics['Total Stores'] = str(0 if totalStores[0] is None else totalStores[0])

                totalSales = list(cursor.execute(self.config.dataHubTotalSalesQuery[day]).fetchone())
                salesDatahubMetrics['Total Sales $'] = str(0 if totalSales[0] is None else totalSales[0])

                print(salesDatahubMetrics['Total Stores'], salesDatahubMetrics['Total Sales $'])

            return salesDatahubMetrics
        except Exception as e:
            self.logger.exception(str(e))
