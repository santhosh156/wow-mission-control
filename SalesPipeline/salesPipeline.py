import json
import sys
from salesStore import SalesStore
from salesEsc  import SalesEsc
from salesAptos import SalesAptos
from salesDataHub import SalesDataHub
from salesEdw import SalesEdw
from salesDailyReads import SalesDailyReads

class SalesPipeline():
    """ Class with variables and methods for Sales Pipeline """

    def __init__(self, logger, config, generalUtils, dbUtils):
        """
            name:	__init__
            desc:	SalesPipeline constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils
        self.generalUtils = generalUtils
        self.salesStore = SalesStore(logger, config, generalUtils)
        self.salesEsc = SalesEsc(logger, config, generalUtils)
        self.salesAptos = SalesAptos(logger, config, dbUtils)
        self.salesDataHub = SalesDataHub(logger, config, dbUtils)
        self.salesEdw = SalesEdw(logger, config, dbUtils)
        self.salesDailyReads = SalesDailyReads(logger, config, dbUtils)

    def build_sales_output(self):
        """
            name:	build_sales_output
            desc:	Method that builds the output sales pipeline
            args:	None
            return:	Sales pipeline output in JSON format
        """
        try:
            salesPipelineOutput = []
            self.logger.info("Started - Building sales pipeline output")
            salesPipelineOutput.append(self.salesStore.get_store_sales_metrics(self.config.salesYesterday))
            salesPipelineOutput.append(self.salesEsc.get_esc_sales_metrics(self.config.salesYesterday))
            salesPipelineOutput.append(self.salesAptos.get_aptos_sales_metrics(self.config.salesYesterday))
            salesPipelineOutput.append(self.salesDataHub.get_datahub_sales_metrics(self.config.salesYesterday))
            salesPipelineOutput.append(self.salesEdw.get_edw_sales_metrics(self.config.salesYesterday))
            salesPipelineOutput.append(self.salesDailyReads.get_dailyreads_sales_metrics(self.config.salesYesterday))
            salesPipelineOutput.append(self.salesStore.get_store_sales_metrics(self.config.salesToday))
            salesPipelineOutput.append(self.salesEsc.get_esc_sales_metrics(self.config.salesToday))
            salesPipelineOutput.append(self.salesAptos.get_aptos_sales_metrics(self.config.salesToday))
            salesPipelineOutput.append(self.salesDataHub.get_datahub_sales_metrics(self.config.salesToday))
            salesPipelineOutput.append(self.salesEdw.get_edw_sales_metrics(self.config.salesToday))
            salesPipelineOutput.append(self.salesDailyReads.get_dailyreads_sales_metrics(self.config.salesToday))

            return json.dumps(salesPipelineOutput)
            self.logger.info("Completed - Building sales pipeline output")
        except Exception as e:
            self.logger.exception(str(e))
