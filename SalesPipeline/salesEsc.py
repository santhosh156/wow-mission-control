import os

class SalesEsc():
    """ Class with variables and methods for ESC sales """

    def __init__(self, logger, config, genUtils):
        """
            name:	__init__
            desc:	SalesEsc constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.genUtils = genUtils

    def get_esc_sales_metrics(self, day):
        """
            name:	get_esc_sales_metrics_yesterday
            desc:	Method that gets all the esc sales metrics for yesterday
            args:	None
            return:	esc sales metrics dictionary
        """
        try:
            salesEscMetrics = {}

            if day:
                salesEscMetrics['BubbleId'] = self.config.escBubbleId + self.config.salesBubbleToday
            else:
                salesEscMetrics['BubbleId'] = self.config.escBubbleId + self.config.salesBubbleYesterday

            salesEscMetrics['Application'] = self.config.escApplication
            salesEscMetrics['Status'] = 'AVAILABLE'

            salesEscMetrics['Total Queues'] = "NA"
            salesEscMetrics['Inactive Queues'] = "NA"

            return salesEscMetrics
        except Exception as e:
            self.logger.exception(str(e))
