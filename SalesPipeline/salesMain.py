import sys
import os
import logging
import logging.handlers
import datetime
import json
from salesConfig import SalesConfig
from salesPipeline import SalesPipeline
import time

#Instantiating config to load app configurations
config = SalesConfig()

sys.path.append(config.libDir)
from dbutils import DBUtils
from utils import Utils

# creating logger
logger = logging.getLogger(config.appName)

# setting up logging level
loggingLevel = logging.DEBUG

# create log formatter
formatter = logging.Formatter(fmt=config.appLogFormat, datefmt=config.appLogDateFormat)

# create file handler and set level to debug
handler = logging.handlers.TimedRotatingFileHandler(config.applogFile, when="W0", interval=7, backupCount=30)
handler.setFormatter(formatter)
handler.setLevel(loggingLevel)
logger.addHandler(handler)
logger.setLevel(loggingLevel)

# Instantiate the dashboard
generalUtils = Utils(logger, config)

# Instantiate the dashboard
dbUtils = DBUtils(logger, config)

# Instantiate the dashboard
salesPipeline = SalesPipeline(logger, config, generalUtils, dbUtils)

try:
    while True:
        logger.info("Started - Sales pipeline ")

        # Loading the sales pipeline data json template to a variable
        salesData = json.load(open('sales_pipeline.json'))
        salesData = dict(salesData)

        # Loading the latest sales pipeline data json file to the same variable
        salesData["results"] = json.loads(salesPipeline.build_sales_output())
        print(salesData)
        # Writes all the sales metrics from SalesPipeline objects the JSON to be consumed by UI
        with open(config.salesJsonFile, 'w') as outFile:
            outFile.write(json.dumps(salesData, indent=4))
            logger.debug("Sales pipeline data written to JSON file")

        logger.info("Completed - Sales pipeline ")
        time.sleep(300);
except Exception as e:
    logger.error(str(e))
