import os

class SalesAptos():
    """ Class with variables and methods for Aptos sales """

    def __init__(self, logger, config, dbUtils):
        """
            name:	__init__
            desc:	SalesAptos constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils

    def get_aptos_sales_metrics(self, day):
        """
            name:	get_aptos_sales_metrics
            desc:	Method that gets all the aptos sales metrics
            args:	None
            return:	aptos sales metrics dictionary
        """
        try:
            salesAptosMetrics = {}

            if day:
                salesAptosMetrics['BubbleId'] = self.config.aptosBubbleId + self.config.salesBubbleToday
            else:
                salesAptosMetrics['BubbleId'] = self.config.aptosBubbleId + self.config.salesBubbleYesterday

            salesAptosMetrics['Application'] = self.config.aptosApplication
            salesAptosMetrics['Status'] = 'AVAILABLE'

            conn = self.dbUtils.db_connect_mssql(self.config.aptosDbServer, self.config.aptosDatabase)
            with conn:
                cursor = conn.cursor()

                storesOpened = list(cursor.execute(self.config.aptosTotalStoresQuery[day]).fetchone())
                salesAptosMetrics['Stores Opened'] = str(storesOpened[0])

                totalSales = list(cursor.execute(self.config.aptosTotalSalesQuery[day]).fetchone())
                salesAptosMetrics['Total Sales $'] = str(totalSales[0])

                zeroSalesStoreCount = cursor.execute(self.config.aptosNumZeroSalesStoreQuery[day]).fetchone()

                if zeroSalesStoreCount is None:
                    salesAptosMetrics['Zero Sales Stores'] = str(0)
                else:
                    salesAptosMetrics['Zero Sales Stores'] = list(zeroSalesStoreCount)[0]

            return salesAptosMetrics
        except Exception as e:
            self.logger.exception(str(e))
