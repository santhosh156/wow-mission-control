import os

class SalesStore():
    """ Class with variables and methods for Store sales """

    def __init__(self, logger, config, genUtils):
        """
            name:	__init__
            desc:	SalesStore constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.genUtils = genUtils

    def get_store_sales_metrics(self, day):
        """
            name:	get_store_sales_metrics_yesterday
            desc:	Method that gets all the store sales metrics for yesterday
            args:	None
            return:	store sales metrics dictionary
        """
        try:
            salesStoreMetrics = {}

            if day:
                salesStoreMetrics['BubbleId'] = self.config.storeBubbleId + self.config.salesBubbleToday
            else:
                salesStoreMetrics['BubbleId'] = self.config.storeBubbleId + self.config.salesBubbleYesterday

            salesStoreMetrics['Application'] = self.config.storeApplication
            salesStoreMetrics['Status'] = 'AVAILABLE'

            salesStoreMetrics['Total Servers'] = "NA"
            salesStoreMetrics['Inactive Servers'] = "NA"

            return salesStoreMetrics
        except Exception as e:
            self.logger.exception(str(e))
