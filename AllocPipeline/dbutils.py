import pyodbc
import cx_Oracle
import datetime, time

class DBUtils:
	""" Class with variables and methods for all database actions """

	def __init__(self, logger, config):
		"""
			name:	__init__
			desc:	DBUtils constructor - Logger and config objects passed from main
			args:	logger - logger object from main
				  	config - config object from main
			return:	None
		"""
		self.logger = logger
		self.config = config

	def db_connect_mssql(self, server, database):
		"""
			name:	db_connect_mssql
			desc:	Method to connect to SQLSERVER database
			args:	server - MSSQL database server
				  	database - Database to connect
			return:	None
		"""
		try:
			connectionString = "Driver={SQL Server};Server=" + server + ";Database=" + database + ";Trusted_Connection=yes;" #+ ";uid=FBLAN" + '\\' + "zz_kumars" + ";pwd=" + self.config.sqlserverPassword
			connection = pyodbc.connect(connectionString)
			self.logger.debug("Connected to {}".format(server))
			return connection
		except Exception as e:
			self.logger.exception(str(e))

	def db_connect_oracle(self, server, port, username, password, serviceName=None, sid=None ):
		"""
			name:	db_connect_oracle
			desc:	Method to connect to Oracle database
			args:	server - Oracle database server
				  	database - Database to connect
			return:	None
		"""
		try:
			if serviceName != None:
				dsn_tns = cx_Oracle.makedsn(server, port, service_name=serviceName)
			elif sid != None:
				dsn_tns = cx_Oracle.makedsn(server, port, sid=sid)
			else:
				raise cx_Oracle.DatabaseError
				
			connection = cx_Oracle.connect(user=username, password=password, dsn=dsn_tns)
			self.logger.debug("Connected to {}".format(server))
			return connection
		except Exception as e:
			self.logger.exception(str(e))
