
class AllocIp():
    """ Class with variables and methods for IP allocation """

    def __init__(self, logger, config, genUtils, dbUtils):
        """
            name:	__init__
            desc:	AllocIp constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils
        self.genUtils = genUtils

    def get_ip_alloc_metrics(self, dcId):
        """
            name:	get_dc4_ip_alloc_metrics
            desc:	Method that gets all the ip allocation metrics for DC4 - Atlanta
            args:	None
            return:	ip allocation metrics dictionary for DC4 - Atlanta
        """
        try:
            ipAllocMetrics = {}
            index = int(dcId) - 2

            ipAllocMetrics['BubbleId'] = self.config.ipBubbleId[index]
            ipAllocMetrics['Application'] = self.config.ipApplication[index]
            ipAllocMetrics['Status'] = 'available'

            # Connecting to LD_TRANSFER table in phsql01 to get ip-dc4 allocation details
            conn = self.dbUtils.db_connect_mssql(self.config.ipDbHost, self.config.ipDatabase)
            cursor = conn.cursor()
            result = cursor.execute(self.config.ipAllocQuery[index]).fetchone()
            metrics = list(result)

            ipAllocMetrics['Allocations Processed'] = "NA"#str(metrics[0])
            ipAllocMetrics['Units Processed'] = "NA"#str(0 if metrics[1] is None else metrics[1])

            # Getting the pending files from IP - /SCALE/00004/Allocation/
            ipAllocMetrics['Unprocessed Alloc Files'] = str(AllocIp.get_pending_alloc_files(self, self.config.ipOutgoingPath[index]))
            return ipAllocMetrics
        except Exception as e:
            self.logger.exception(str(e))

    def get_pending_alloc_files(self, outgoingPath):
        """
            name:	get_pending_alloc_files
            desc: 	Method that gets the count of pending allocation files from IP
            args:	None
            return:	count of pending allocation files
        """
        try:
            self.logger.info("Started - Retrieval of count pemding allocation files from IP ")

            # Retrieving the count of pending files by sftp connecting to IP
            fileCount = self.genUtils.sftp_get_file_count(self.config.ipHost, self.config.ipUsername, self.config.ipPassword, outgoingPath, self.config.ipAllocFileFormat)

            self.logger.info("Completed - Retrieval of count allocation files from IP ")
            # Returning the count of pending files by sftp connecting to IP
            return fileCount
        except Exception as e:
            self.logger.exception(str(e))
