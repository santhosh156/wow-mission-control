import os

class AllocTrafficCop():
    """ Class with variables and methods for JDA allocation """

    def __init__(self, logger, config, genUtils, dbUtils):
        """
            name:	__init__
            desc:	AllocTrafficCop constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils
        self.genUtils = genUtils

    def get_traffic_alloc_metrics(self, dcId):
        """
            name:	get_traffic_alloc_metrics
            desc:	Method that gets all the traffic cop allocation metrics
            args:	None
            return:	traffic cop allocation metrics dictionary
        """
        try:
            trafficAllocMetrics = {}
            index = int(dcId) - 2

            trafficAllocMetrics['BubbleId'] = self.config.trafficBubbleId[index]
            trafficAllocMetrics['Application'] = self.config.trafficApplication
            trafficAllocMetrics['Status'] = 'available'

            conn = self.dbUtils.db_connect_mssql(self.config.trafficDbHost, self.config.trafficDatabase)
            cursor = conn.cursor()
            result = cursor.execute(self.config.trafficAllocQuery[index]).fetchone()
            metrics = list(result)

            trafficAllocMetrics['Allocations Processed'] = str(metrics[0])
            trafficAllocMetrics['Units Processed'] = "NA"#str(0 if metrics[1] is None else metrics[1])
            trafficAllocMetrics['Unprocessed Alloc Files'] = str(self.genUtils.get_file_count('Traffic cop', self.config.trafficIncomingPath, self.config.jdaAllocFileFormat))
            return trafficAllocMetrics
        except Exception as e:
            self.logger.exception(str(e))
