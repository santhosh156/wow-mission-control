import datetime, time
import os
import paramiko
import pysftp

class Utils:
    """ Class with variables and methods for all general utilities """

    def __init__(self, logger, config):
        """
            name:	__init__
            desc:	Utils constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config

    def get_file_count(self, system, filePath, fileExtension):
        """
            name:	get_file_count
            desc:	Method that gets the file count in a folder
            args:	system - system where files are checked
                    filePath - path of the Files
                    fileExtension - extension of the files to check
            return:	number of files to be processed
        """
        try:
            self.logger.info("Started - Retrieval of pending files from {} ".format(system))

            # Fetching all the files in destination directory ending with file extnsion
            fileCount = len([name for name in os.listdir(filePath) if name.endswith(fileExtension)])
            return fileCount

            self.logger.info("Completed - Retrieval of pending files from {} ".format(system))
        except Exception as e:
            self.logger.exception(str(e))

    def sftp_get_file_count(self, host, username, password, remotePath, fileExtension):
        """
            name:	sftp_get_file_count
            desc:	method to get the file count in a remotepath thru SFTP
            args:	host - hostname of the source server
                    user - username to connect
                    password - password of the user to connect
                    remotePath - remote path the SFTPed files from
            return: filesCount - number of files in the remotepath
        """
        try:
            self.logger.debug("Begin - sftp_get_file_count()")
            cnopts = pysftp.CnOpts()
            cnopts.hostkeys = None

            with pysftp.Connection(host=host, username=username, password=password, cnopts=cnopts) as sftp:
                self.logger.info("SFTP Connection established with {}".format(host))

                # change the current working dir to remote path
                sftp.chdir(remotePath)
                fileCount = len([name for name in sftp.listdir() if name.endswith(fileExtension)])
                return fileCount

            self.logger.info("{} file count retrieved from {} in {}".format(fileExtension, remotePath, host))
            self.logger.info("SFTP Connection closed with {}".format(host))
            self.logger.debug("End - sftp_get_file_names()")
        except Exception as e:
            self.logger.exception(str(e))

    def run_unix_command(self, host, port, username, password, command):
        """
            name:	run_unix_command
            desc: 	Method that runs the unix command and returns the output
            args:	command - command to be run in unix box
            return:	Output of the command
        """
        try:
            self.logger.info("Started - Running command: {} in server: {}".format(command, host))
            self.logger.debug("Checking for count of the files in JDA folder in server: {}".format(command, host))
            # Calling ssh client method
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            # Connecting to JDA host using usernare and password
            ssh.connect(host, username=username, password=password, port=port)
            self.logger.info("Connection established with {}".format(host))
            # Opening a session with JDA server
            channel = ssh.get_transport().open_session()
            # Running command in JDA host
            channel.exec_command(command)
            # Reading output after running the command
            stdout = channel.makefile().read()
            channel.close()
            # Closing a session with JDA server
            ssh.close()
            return stdout
            self.logger.info("Connection closed with {}".format(host))
        except Exception as e:
            self.logger.exception(str(e))
