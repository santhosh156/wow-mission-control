import json
from allocJda import AllocJda
from allocTrafficCop import AllocTrafficCop
from allocIp import AllocIp
from allocAzure import AllocAzure
from allocMif import AllocMif
from allocWmos import AllocWmos
from allocScale import AllocScale
import sys

class AllocPipeline():
    """ Class with variables and methods for Allocation Pipeline """

    def __init__(self, logger, config, generalUtils, dbUtils):
        """
            name:	__init__
            desc:	AllocPipeline constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils
        self.generalUtils = generalUtils
        self.allocJda = AllocJda(logger, config, self.generalUtils, self.dbUtils)
        self.allocTraffic = AllocTrafficCop(logger, config, self.generalUtils, self.dbUtils)
        self.allocIp = AllocIp(logger, config, self.generalUtils, self.dbUtils)
        self.allocAzure = AllocAzure(logger, config, self.dbUtils)
        self.allocMif = AllocMif(logger, config, self.generalUtils)
        self.allocScale = AllocScale(logger, config, self.generalUtils, self.dbUtils)
        self.allocWmos = AllocWmos(logger, config, self.dbUtils)

    def build_alloc_output(self):
        """
            name:	build_alloc_output
            desc:	Method that builds the output allocation pipeline
            args:	None
            return:	Allocation pipeline output in JSON format
        """
        try:
            allocPipelineOutput = []
            self.logger.info("Started - Building allocation pipeline output")
            allocPipelineOutput.append(self.allocJda.get_jda_alloc_metrics(self.config.oliveBranch))
            allocPipelineOutput.append(self.allocTraffic.get_traffic_alloc_metrics(self.config.oliveBranch))
            allocPipelineOutput.append(self.allocIp.get_ip_alloc_metrics(self.config.oliveBranch))
            allocPipelineOutput.append(self.allocScale.get_scale_alloc_metrics(self.config.oliveBranch))
            allocPipelineOutput.append(self.allocJda.get_jda_alloc_metrics(self.config.predricktown))
            allocPipelineOutput.append(self.allocTraffic.get_traffic_alloc_metrics(self.config.predricktown))
            allocPipelineOutput.append(self.allocIp.get_ip_alloc_metrics(self.config.predricktown))
            allocPipelineOutput.append(self.allocScale.get_scale_alloc_metrics(self.config.predricktown))
            allocPipelineOutput.append(self.allocJda.get_jda_alloc_metrics(self.config.atlanta))
            allocPipelineOutput.append(self.allocTraffic.get_traffic_alloc_metrics(self.config.atlanta))
            allocPipelineOutput.append(self.allocIp.get_ip_alloc_metrics(self.config.atlanta))
            allocPipelineOutput.append(self.allocAzure.get_azure_alloc_metrics())
            allocPipelineOutput.append(self.allocMif.get_mif_alloc_metrics())
            allocPipelineOutput.append(self.allocWmos.get_wmos_alloc_metrics())
            return json.dumps(allocPipelineOutput)
            self.logger.info("Completed - Building allocation pipeline output")
        except Exception as e:
            self.logger.exception(str(e))
