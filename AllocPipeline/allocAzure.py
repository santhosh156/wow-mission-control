import os

class AllocAzure():
    """ Class with variables and methods for Azure allocation """

    def __init__(self, logger, config, dbUtils):
        """
            name:	__init__
            desc:	AllocAzure constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils

    def get_azure_alloc_metrics(self):
        """
            name:	get_azure_alloc_metrics
            desc:	Method that gets all the azure allocation metrics
            args:	None
            return:	azure allocation metrics dictionary
        """
        try:
            azureAllocMetrics = {}
            azureAllocMetrics['BubbleId'] = self.config.azureBubbleId
            azureAllocMetrics['Application'] = self.config.azureApplication
            azureAllocMetrics['Status'] = 'available'

            azureAllocMetrics['Allocations Processed'] = "NA"#str(0)
            azureAllocMetrics['Units Processed'] = "NA"#str(0)
            azureAllocMetrics['Unprocessed Alloc Files'] = "NA"#str(AllocAzure.get_pending_alloc_files(self))
            return azureAllocMetrics
        except Exception as e:
            self.logger.exception(str(e))

    def get_pending_alloc_files(self):
        """
            name:	get_pending_alloc_files
            desc:	Method that gets all the pending files in azure  Incoming folder
            args:	None
            return:	number of pending files to be processed in azure
        """
        try:
            return 0
            '''self.logger.info("Started - Retrieval of count allocation files from azure  ")

            # Assigning the target file path location
            serverFilePath =  self.config.azureIncomingPath
            count = 0

            # Fetching all the files in destination directory ending with .dat
            for file in os.listdir(serverFilePath):
                if file.endswith(".dat"):
                    # Incrementing the count for each .dat file found in the destination folder
                    count += 1

            self.logger.info("Completed - Retrieval of count allocation files from azure  ")

            # Returning the total no of files with extension .dat
            return count'''
        except Exception as e:
            self.logger.exception(str(e))
