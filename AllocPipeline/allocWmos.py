import paramiko
from datetime import datetime

class AllocWmos():
    """ Class with variables and methods for WMOS allocation """

    def __init__(self, logger, config, dbUtils):
        """
            name:	__init__
            desc:	AllocWmos constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils

    def get_wmos_alloc_metrics(self):
        """
            name:	get_wmos_alloc_metrics
            desc:	Method that gets all the wmos allocation metrics
            args:	None
            return:	wmos allocation metrics dictionary
        """
        try:
            wmosAllocMetrics = {}
            wmosAllocMetrics['BubbleId'] = self.config.wmosBubbleId
            wmosAllocMetrics['Application'] = self.config.wmosApplication
            wmosAllocMetrics['Status'] = 'available'

            conn = self.dbUtils.db_connect_oracle(self.config.wmosDbHost, self.config.wmosDbPort, self.config.wmosDbUsername, self.config.wmosDbPassword, sid=self.config.wmosDbSid)
            cursor = conn.cursor()
            cursor.execute(self.config.wmosAllocQuery)
            for row in cursor:
                wmosAllocMetrics['Allocations Processed'] = str(row[0])
                wmosAllocMetrics['Units Processed'] = str(0 if row[1] is None else row[1])

            wmosAllocMetrics['Unprocessed Alloc Files'] = "NA"#str(0)
            return wmosAllocMetrics
        except Exception as e:
            self.logger.exception(str(e))
