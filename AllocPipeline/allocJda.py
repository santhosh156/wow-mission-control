import paramiko
from datetime import datetime

class AllocJda():
    """ Class with variables and methods for JDA allocation """

    def __init__(self, logger, config, genUtils, dbUtils):
        """
            name:	__init__
            desc:	AllocJda constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils
        self.genUtils = genUtils

    def get_jda_alloc_metrics(self, dcId):
        """
            name:	get_jda_alloc_metrics
            desc:	Method that gets all the jda allocation metrics
            args:	None
            return:	jda allocation metrics dictionary
        """
        try:
            jdaAllocMetrics = {}
            index = int(dcId) - 2

            jdaAllocMetrics['BubbleId'] = self.config.jdaBubbleId[index]
            jdaAllocMetrics['Application'] = self.config.jdaApplication
            jdaAllocMetrics['Status'] = 'available'

            conn = self.dbUtils.db_connect_oracle(self.config.jdaHost, self.config.jdaDbPort, self.config.jdaDbUsername, self.config.jdaDbPassword, serviceName=self.config.jdaServiceName)
            cursor = conn.cursor()
            cursor.execute(self.config.jdaAllocCountQuery[index])
            for row in cursor:
                jdaAllocMetrics['Allocations Created'] = str(row[0])

            conn = self.dbUtils.db_connect_oracle(self.config.jdaHost, self.config.jdaDbPort, self.config.jdaDbUsername, self.config.jdaDbPassword, serviceName=self.config.jdaServiceName)
            cursor = conn.cursor()
            cursor.execute(self.config.jdaimplodedQtyQuery[index])
            for row in cursor:
                jdaAllocMetrics['Units Processed'] = str(0 if row[0] is None else row[0])

            jdaAllocMetrics['Unprocessed Alloc Files'] = str(AllocJda.get_pending_alloc_files(self))
            return jdaAllocMetrics
        except Exception as e:
            self.logger.exception(str(e))

    def get_alloc_count(self):
        """
            name:	get_alloc_count
            desc:	Method that gets the count of allocations created in JDA
            args:	None
            return:	jda allocation count
        """
        try:
            conn = self.dbUtils.db_connect_oracle(self.config.jdaHost, self.config.jdaDbPort, self.config.jdaServiceName, self.config.jdaDbUsername, self.config.jdaDbPassword)
            #today = datetime.now().strftime("%Y%m%d")
            #jdaAllocReport = self.config.jdaAllocReport + today + '.txt'
            #return sum(1 for line in open(jdaAllocReport))
            cursor = conn.cursor()
            cursor.execute(self.config.jdaAllocQuery)
            #for row in cursor:
                #row[0], row[1], row[2]
        except Exception as e:
            self.logger.exception(str(e))

    def get_pending_alloc_files(self):
        """
            name:	get_pending_alloc_files
            desc: 	Method that gets the count of allocation files from jda
            args:	None
            return:	count of allocation files
        """
        try:
            self.logger.info("Started - Retrieval of count allocation files from JDA ")
            # Creating command to get the count of files in jdaOutgoingPath
            command = self.config.unixLSCommand + " " + self.config.jdaOutgoingPath + "*" + self.config.jdaAllocFileFormat + " | " + self.config.unixCountCommand
            # Calling run_unix_command method to run the command
            stdout = self.genUtils.run_unix_command(self.config.jdaHost, self.config.jdaPort, self.config.jdaUsername, self.config.jdaPassword, command)
            self.logger.debug("After running the Unix command to retrieve files count".format(command))
            output = stdout.decode('utf-8').split('\n')[:-1]
            self.logger.info("Completed - Retrieval of count allocation files from JDA ")
            # Returning the count of files to main method
            return int(output[0])
        except Exception as e:
            self.logger.exception(str(e))
