import paramiko
from datetime import datetime

class AllocMif():
    """ Class with variables and methods for MIF allocation """

    def __init__(self, logger, config, genUtils):
        """
            name:	__init__
            desc:	AllocMif constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.genUtils = genUtils

    def get_mif_alloc_metrics(self):
        """
            name:	get_mif_alloc_metrics
            desc:	Method that gets all the mif allocation metrics
            args:	None
            return:	mif allocation metrics dictionary
        """
        try:
            mifAllocMetrics = {}
            mifAllocMetrics['BubbleId'] = self.config.mifBubbleId
            mifAllocMetrics['Application'] = self.config.mifApplication
            mifAllocMetrics['Status'] = 'available'

            mifAllocMetrics['Allocations Processed'] = "NA"#str(0)#str(AllocMif.get_processed_alloc_files(self))
            mifAllocMetrics['Units Processed'] = "NA"#str(0)

            mifAllocMetrics['Unprocessed Alloc Files'] = str(AllocMif.get_pending_alloc_files(self))
            return mifAllocMetrics
        except Exception as e:
            self.logger.exception(str(e))

    def get_processed_alloc_files(self):
        """
            name:	get_processed_alloc_files
            desc: 	Method that gets the count of processed allocation files from MIF
            args:	None
            return:	count of allocation files processed
        """
        try:
            self.logger.info("Started - Retrieval of count of processed allocation files from MIF ")

            today = datetime.now().strftime("%Y-%m-%d") #2019-11-19
            # Creating command to get the count of files in jdaOutgoingPath
            command = self.config.unixLSCommand + " " + self.config.mifCompletedPath + "*" + today + "*" + self.config.mifAllocFileFormat + " | " + self.config.unixCountCommand
            # Calling run_unix_command method to run the command
            stdout = self.genUtils.run_unix_command(self.config.mifHost, self.config.mifPort, self.config.mifUsername, self.config.mifPassword, command)
            self.logger.debug("After running the Unix command to retrieve files count".format(command))
            output = stdout.decode('utf-8').split('\n')[:-1]
            self.logger.info("Completed - Retrieval of count allocation files from JDA ")
            # Returning the count of files to main method
            return output[0]
        except Exception as e:
            self.logger.exception(str(e))

    def get_pending_alloc_files(self):
        """
            name:	get_pending_alloc_files
            desc: 	Method that gets the count of allocation files from mif
            args:	None
            return:	count of allocation files
        """
        try:
            self.logger.info("Started - Retrieval of count allocation files from mif ")

            today = datetime.now().strftime("%Y-%m-%d") #2019-11-19

            # Creating command to get the count of files in mifOutgoingPath
            command = self.config.unixLSCommand + " " + self.config.mifIncomingPath + "*" + today + "*" + self.config.mifAllocFileFormat + " | " + self.config.unixCountCommand
            # Calling run_unix_command method to run the command
            stdout = self.genUtils.run_unix_command(self.config.mifHost, self.config.mifPort, self.config.mifUsername, self.config.mifPassword, command)
            self.logger.debug("After running the Unix command to retrieve files count".format(command))
            output = stdout.decode('utf-8').split('\n')[:-1]
            self.logger.info("Completed - Retrieval of count allocation files from mif ")
            # Returning the count of files to main method
            return int(output[0])
        except Exception as e:
            self.logger.exception(str(e))
