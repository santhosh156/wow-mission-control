import os

class AllocScale():
    """ Class with variables and methods for SCALE allocation """

    def __init__(self, logger, config, genUtils, dbUtils):
        """
            name:	__init__
            desc:	AllocScale constructor - Logger and config objects passed from main
            args:	logger - logger object from main
                    config - config object from main
            return:	None
        """
        self.logger = logger
        self.config = config
        self.dbUtils = dbUtils
        self.genUtils = genUtils

    def get_scale_alloc_metrics(self, dcId):
        """
            name:	get_scale_alloc_metrics
            desc:	Method that gets all the scale allocation metrics
            args:	None
            return:	scale allocation metrics dictionary
        """
        try:
            scaleAllocMetrics = {}
            index = int(dcId) - 2

            scaleAllocMetrics['BubbleId'] = self.config.scaleBubbleId[index]
            scaleAllocMetrics['Application'] = self.config.scaleApplication
            scaleAllocMetrics['Status'] = 'available'

            conn = self.dbUtils.db_connect_mssql(self.config.scaleDbHost[index], self.config.scaleDatabase)
            cursor = conn.cursor()
            result = cursor.execute(self.config.scaleAllocQuery).fetchone()
            metrics = list(result)

            allocInHold = self.genUtils.get_file_count('Scale', self.config.scaleHoldPath[index], self.config.ipAllocFileFormat)
            allocPending = self.genUtils.get_file_count('Scale', self.config.scaleIncomingPath[index], self.config.ipAllocFileFormat)
            allocProcessing = self.genUtils.get_file_count('Scale', self.config.scaleIncomingPath[index], self.config.scaleTempFileFormat)

            scaleAllocMetrics['Total Allocations'] = str(metrics[0])
            scaleAllocMetrics['Total Allocated Units'] = str(0 if metrics[1] is None else metrics[1])
            scaleAllocMetrics['Units in Pool'] = str(0 if metrics[2] is None else metrics[2])
            scaleAllocMetrics['Units Available to Wave'] = str(0 if metrics[3] is None else metrics[3])
            scaleAllocMetrics['Unprocessed Alloc Files'] = str(allocInHold + allocPending + allocProcessing)
            return scaleAllocMetrics
        except Exception as e:
            self.logger.exception(str(e))
