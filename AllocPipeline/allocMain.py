import sys
import os
import logging
import logging.handlers
import datetime
import json
from allocConfig import AllocConfig
from allocPipeline import AllocPipeline
import time

#Instantiating config to load app configurations
config = AllocConfig()

sys.path.append(config.libDir)
from dbutils import DBUtils
from utils import Utils

# creating logger
logger = logging.getLogger(config.appName)

# setting up logging level
loggingLevel = logging.DEBUG

# create log formatter
formatter = logging.Formatter(fmt=config.appLogFormat, datefmt=config.appLogDateFormat)

# create file handler and set level to debug
handler = logging.handlers.TimedRotatingFileHandler(config.applogFile, when="W0", interval=7, backupCount=30)
handler.setFormatter(formatter)
handler.setLevel(loggingLevel)
logger.addHandler(handler)
logger.setLevel(loggingLevel)

# Instantiate the dashboard
generalUtils = Utils(logger, config)

# Instantiate the dashboard
dbUtils = DBUtils(logger, config)

# Instantiate the dashboard
allocPipeline = AllocPipeline(logger, config, generalUtils, dbUtils)

try:
    while True:
        logger.info("Started - Allocation pipeline ")

        # Loading the previous allocation pipeline data json file to a variable
        allocData = json.load(open('alloc_pipeline.json'))
        allocData = dict(allocData)

        # Loading the latest allocation pipeline data json file to the same variable
        allocData["results"] = json.loads(allocPipeline.build_alloc_output())

        # Writes all the allocation metrics from AllocPipeline objects the JSON to be consumed by UI
        with open(config.allocJsonFile, 'w') as outFile:
            outFile.write(json.dumps(allocData, indent=4))
            logger.debug("Allocation pipeline data written to JSON file")

        logger.info("Completed - Allocation pipeline ")
        time.sleep(120);
except Exception as e:
    logger.error(str(e))
